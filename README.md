# Tareas Becarios 2019

En este repositorio tendremos las tareas.

Favor de revisar la [lista de issues] y el documento del [temario] para ver las tareas que se han dejado

Las tareas se entregan con _merge request_ a la rama `entregas`

Guias de `Markdown`:

+ <https://docs.gitlab.com/ce/user/markdown.html>
+ <https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/>
+ <https://about.gitlab.com/2018/08/17/gitlab-markdown-tutorial/>
+ <https://docs.gitlab.com/ee/development/documentation/styleguide.html>

--------------------------------------------------------------------------------

[lista de issues]: https://gitlab.com/PBSC-AdminUNIX/2019/tareas/issues
[temario]: https://tinyurl.com/Temario-AdminUNIX-2019
